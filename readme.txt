Para ejecutar tendriamos que seguir este orden
1) namespace (este yaml crea todos los namespaces por nosotros)
2) pv
3) pvc
4) deployments (son los yaml que no dicen service)
5) service
6) ingress

tambien se podria ejecutar con un kubectl apply -f . // pero para ello
 hay que renombrar los archivos anteponiendo el numero de orden



Aclaraciones:

modificar el ip del ingress, poner la ip del nodo donde corre
 (activar addons de ingrees si se esta en minikube)

reviews tiene problemas al conectarse con ratings, por lo que no
 puedo comprobar si funciona ratings y mongo.

si hago un kubectl exec en productpage puedo tirar un ping hasta
 mongodb-0.mongodb-service.mongodb.svc.cluster.local
 por lo que no parece ser un problema del networking de mongo.

En mongodb.yml NO defini el volumen como un VolumenClaimTemplate,
 sino simpletemente le dije especificamente que volumen queria que tomara,
  por lo que esta solucion no es escalable. Entrando al describe de mongo veo que si toma el volumen. 

Deje todas las variables de entorno HARD-codeadas,
mi idea era hacer funcionar todo antes de agregar mas archivos
y variables asi mantenia las cosas mas simples hasta que corra todo. 


